import UI.Plateau;

import java.io.DataInputStream;
import java.io.DataOutputStream;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.JOptionPane;

public class Client {

    private Plateau plateau;

    public Client(Plateau plateau) {
        this.plateau = plateau;
    }

    public void run() throws IOException {

        Socket sock = null;
        sock = new Socket("172.20.10.2", 8080);
        while (true) {
            try {
                DataOutputStream out = new DataOutputStream(sock.getOutputStream());
                this.plateau.setOut(out);
                DataInputStream in= new DataInputStream(sock.getInputStream());
                this.plateau.setIn(in);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}