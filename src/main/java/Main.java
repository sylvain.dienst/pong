import UI.Plateau;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        Plateau plateau = new Plateau();
        try
        {
            Client client = new Client(plateau);
            plateau.setTitle("Pong Client");
            client.run();
        } catch (Exception e) {
            Server server = new Server(plateau);
            plateau.setTitle("Pong Server");
            server.run();
        }

    }
}
