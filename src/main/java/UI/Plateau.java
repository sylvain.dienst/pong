package UI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Plateau extends JFrame implements KeyListener{

    JPanel global = new JPanel();

    JPanel pane = new JPanel();

    Raquette raquette;
    private DataOutputStream out;

    Raquette remoteR;
    private DataInputStream in;

    public Plateau()
    {
        super("Pong");
        final Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        setSize(dim.width - 100, dim.height- 100);
        setLocation(dim.width/2 - this.getSize().width / 2, dim.height/2 - this.getSize().height / 2);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        global.setLayout(new BorderLayout());
        addKeyListener(this);
        this.raquette = new Raquette(dim.height/4, 1, 1);
        this.remoteR = new Raquette(dim.height/4, this.getWidth()/2-51, 1);
        setResizable(false);
        pane.setLayout(new GridLayout(1,2));
        pane.add(raquette);
        pane.add(remoteR);
        global.add(pane, "Center");
        setContentPane(global);
        setVisible(true);
    }

    public void keyTyped(KeyEvent keyEvent) {

    }

    public void keyPressed(KeyEvent keyEvent) {
        this.raquette.pressed(keyEvent.getKeyCode());
        try {
            this.out.writeInt(this.raquette.getY());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void keyReleased(KeyEvent keyEvent) {

    }

    public void updateRemote() {
        try {
            this.remoteR.move(in.readInt());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setOut(DataOutputStream out) {
        this.out = out;
    }

    public void setIn(DataInputStream in) throws IOException {
        this.in = in;
        System.out.println(this.in.readInt());
        this.updateRemote();
    }
}
