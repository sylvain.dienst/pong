package UI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;

public class Raquette extends JPanel{
    private static final int WIDTH = 50;
    private int height;
    private int x;
    private int y;

    public Raquette(int height, int x, int y) {
        this.height = height;
        this.x = x;
        this.y = y;
    }

    @Override
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        g.fillRect(this.x, this.y, WIDTH, this.height);
        g.setColor(Color.black);
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void pressed(int keyCode) {
        if (keyCode == KeyEvent.VK_UP)
        {
            this.y -= 5;
            this.setLocation(this.x, this.y);
        }
        if (keyCode == KeyEvent.VK_DOWN)
        {
            this.y += 5;
            this.setLocation(this.x, this.y);
        }
    }

    public void move(int y) {
        this.setLocation(this.x, y);
        this.y = y;
    }
}
