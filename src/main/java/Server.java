import UI.Plateau;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import javax.swing.JOptionPane;

public class Server extends Thread{

    Plateau plateau;

    public Server(Plateau plateau) {
        this.plateau = plateau;
    }

    @Override
    public void run(){
        ServerSocket serverSock= null;
        try {
            serverSock = new ServerSocket(8080);
            while (true)
            {
                Socket Sock = serverSock.accept();
                DataOutputStream out = new DataOutputStream(Sock.getOutputStream());
                this.plateau.setOut(out);
                DataInputStream in= new DataInputStream(Sock.getInputStream());
                this.plateau.setIn(in);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}